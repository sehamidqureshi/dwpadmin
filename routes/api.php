<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("/getAllOCProducts", "OpencartController@getAllProducts");
Route::get("/getProductsByFields/{fields}", "OpencartController@getProductsByFields");
Route::get("/getProductsById/{id}", "OpencartController@getProductsById");
Route::get("/getProductsBySlug/{slug}", "OpencartController@getProductsBySlug");
Route::get("/getProductsByManufacturer/{manufacturerId}", "OpencartController@getProductsByManufacturerId");
Route::get("/getProductsByManufacturerLimit/{manufacturerId}/{limit}/{page}", "OpencartController@getProductsByManufacturerLimit");
Route::get("/getProductsByCategoryLimit/{id}/{limit}/{page}", "OpencartController@getProductsByCategoryLimit");
Route::get("/getRelatedProducts/{productId}", "OpencartController@getRelatedProducts");
Route::get("/getAllManufacturer", "OpencartController@getAllManufacturer");
Route::get("/getManufacturer/{manufacturerId}", "OpencartController@getManufacturer");
Route::get("/getProductsByLimit/{limit}/{page}", "OpencartController@getProductsByPageLimit");
Route::get("/getProductsBySearch/{query}", "OpencartController@getProductsBySearch");
Route::get("/getProductsBySearchLimit/{search}/{limit}/{page}", "OpencartController@getProductsBySearchLimit");
Route::get("/getProductsByCategoryId/{id}", "OpencartController@getProductsByCategoryId");
Route::get("/getFeaturedProducts", "OpencartController@getAllFeatureProducts");
Route::get("/getFeaturedProductsByLimit/{limit}", "OpencartController@getAllFeatureProductsByLimit");
Route::get("/getLatestProducts", "OpencartController@getAllLatestProducts");
Route::get("/getLatestProductsByLimit/{limit}", "OpencartController@getLatestProductsByLimit");
Route::get("/getSpecialProducts", "OpencartController@getAllSpecialProducts");
Route::get("/getSpecialProductsByLimit/{limit}", "OpencartController@getAllSpecialProductsByLimit");
Route::get("/getSimpleListOfProducts", "OpencartController@getSimpleListProducts");
Route::get("/getWishlist", "OpencartController@getWishlist");
Route::post("/addProductToWishlist/{productId}", "OpencartController@addProductToWishlist");
Route::delete("/deleteProductFromWishlist/{productId}", "OpencartController@deleteProductFromWishlist");
Route::get("/getBestSellers/{limit}", "OpencartController@getBestSellers");
Route::post("/postProductReview/{productId}", "OpencartController@postProductReview");
Route::post("/addItemsToCart", "OpencartController@addItemsToCart");
Route::get("/getCart", "OpencartController@getCartItems");
Route::post("/updateCartItems", "OpencartController@updateItemsToCart");
Route::get("/deleteCartItem/{productId}", "OpencartController@deleteItemsFromCart");
Route::get("/emptyCart", "OpencartController@emptyCart");

/** Subscriptions **/

Route::get("/subscribenewsletter", "OpencartController@subscribeNewsLetter");
Route::get("/unsubscribenewsletter", "OpencartController@unsubscribeNewsLetter");

/** Address **/
Route::get("/listAddresses", "OpencartController@listAddresses");
Route::post("/addNewAddress", "OpencartController@addNewAddressToCustomer");
Route::get("/getAddressById/{id}", "OpencartController@getAddressById");
Route::post("/updateAddress/{id}", "OpencartController@updateAddress");
Route::delete("/deleteAddress/{id}", "OpencartController@deleteAddress");

/** Account - basic services **/
Route::get("/getAccount", "OpencartController@getAccount");
Route::post("/updateAccountData", "OpencartController@updateAccountData");
Route::post("/login", "OpencartController@login");
Route::post("/forgottenPassword", "OpencartController@forgottenPassword");
Route::post("/changePassword", "OpencartController@changePassword");


/** Countries and Zone **/
Route::get("/getListofCountries", "OpencartController@ListCountries");
Route::get("/getZonesById/{countryId}", "OpencartController@ListZones");


/************ ETL *************/

Route::get("/runetl", "OpencartController@runETL");
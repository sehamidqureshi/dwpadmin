<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use App\Http\Requests\OCRequest;
use DB;

class OpencartController extends Controller
{

    private $authorization;
    private $clientId;
    private $clientSecret;
    private $adminurl;
    
    public function __construct() {
        
        $this->authorization = 'Bearer '.env('DWP_API_AUTH');
        $this->clientId = env('DWP_API_CLIENT_ID');
        $this->clientSecret = env('DWP_API_CLIENT_SECRET');
        $this->adminurl = env('DWP_API_ADMIN_URL');
        
    }
    
    public function getAllProducts()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }
    
    public function getProductsByFields($fields)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/simple/customfields/$fields", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function getProductsBySearch($query)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/search/$query", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }
    
    public function getProductsByPageLimit($limit,$page)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/limit/$limit/page/$page", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    

    
    public function getProductsByManufacturerId($manufacturerId)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/manufacturer/$manufacturerId", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    
    public function getProductsByManufacturerLimit($manufacturerId,$limit,$page)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/manufacturer/$manufacturerId/limit/$limit/page/$page", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }     
    
    
    
    public function getAllManufacturer()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/manufacturers", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    
    public function getManufacturer($manufacturerId)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/manufacturers/$manufacturerId", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    
    public function subscribeNewsLetter()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->put($this->adminurl."/api/rest/newsletter/subscribe", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function unsubscribeNewsLetter()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->put($this->adminurl."/api/rest/newsletter/unsubscribe", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }     
    

    public function getProductsBySearchLimit($search,$limit,$page)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/search/$search/limit/$limit/page/$page", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }      
    
    
    
    public function getAllFeatureProducts()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/featured", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    
    public function getRelatedProducts($productId)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/related/$productId", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }     
    
    public function getAllFeatureProductsByLimit($limit)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/featured/limit/$limit", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }    
    
    public function getProductsById($id)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/$id", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();  
     
    } 

    public function getProductsByCategoryId($id)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/category/$id", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();  
     
    }
    
    public function getProductsByCategoryLimit($id,$limit,$page)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/category/$id/limit/$limit/page/$page", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();  
     
    }    
    
    public function getProductsBySlug($slug)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/slug/$slug", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();  
     
    }    
    
    public function getAllLatestProducts()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/latest", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    
    public function getLatestProductsByLimit($limit)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/latest/limit/$limit", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }     
    
    public function getSimpleListProducts()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/products/simple", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function getWishlist()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/wishlist", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }   
    
    public function addProductToWishlist($productId)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->post($this->adminurl."/api/rest/wishlist/$productId", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
            
        ]);     
        
        return $response->json();
        
    } 
    
    public function listAddresses()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/account/address", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function getAddressById($id)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/account/address/$id", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function deleteAddress($id)
    {
        
        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->delete($this->adminurl."/api/rest/account/address/$id", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }     
    
    public function updateAddress($id, OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->put($this->adminurl."/api/rest/account/address/$id", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'city' => $request->city,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'country_id' => $request->country_id,
            'postcode' => $request->postcode,
            'zone_id' => $request->zone_id,
            'company' => $request->company,
            'default' => $request->default
            
        ]);     
        
        return $response->json();
        
    }      
    
    public function addNewAddressToCustomer(OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->post($this->adminurl."/api/rest/account/address", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'city' => $request->city,
            'address_1' => $request->address_1,
            'address_2' => $request->address_2,
            'country_id' => $request->country_id,
            'postcode' => $request->postcode,
            'zone_id' => $request->zone_id,
            'company' => $request->company,
            'default' => $request->default
            
        ]);     
        
        return $response->json();
        
    }  
    
    public function ListCountries()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/countries", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function ListZones($countryId)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/countries/$countryId", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    
    
    
    
    
    
    
    
    public function deleteProductFromWishlist($productId)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->delete($this->adminurl."/api/rest/wishlist/$productId", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
            
        ]);     
        
        return $response->json();
        
    }     
    
    public function getAllSpecialProducts()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/specials", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function getAllSpecialProductsByLimit($limit)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/specials/limit/$limit", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    }  
    
    public function getBestSellers($limit)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/bestsellers/limit/$limit", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    
    public function postProductReview($productId, OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->post($this->adminurl."/api/rest/products/$productId/review", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret,
            'name' => $request->name,
            'text' => $request->text,
            'rating' => $request->rating
            
        ]);     
        
        return $response->json();
        
    }  
    
    public function addItemsToCart(OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->post($this->adminurl."/api/rest/cart", [
            'product_id' => $request->product_id,
            'quantity' => $request->quantity
            
            
        ]);     
        
        return $response->json();
        
    } 
    
    public function updateItemsToCart(OCRequest $request)
    {
        
        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->put($this->adminurl."/api/rest/cart", [
            "key" => "$request->product_id",
            "quantity" => "$request->quantity"
 
        ]);     
        
        return $response->json();
        
    } 
    
    public function deleteItemsFromCart($key)
    {
        
        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->delete($this->adminurl."/api/rest/cart", [
            
            "key" => $key   
        
        ]);     
        
        return $response->json();
        
    } 
    
    public function emptyCart()
    {
        
        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->delete($this->adminurl."/api/rest/cart/empty");     
        
        return $response->json();
        
    }     
    
    public function getCartItems()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/cart");     
        
        return $response->json();
        
    }    
    
    public function getAccount()
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->get($this->adminurl."/api/rest/account", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret
        ]);     
        
        return $response->json();
        
    } 
    
    public function updateAccountData(OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->put($this->adminurl."/api/rest/account", [
            'client id' => $this->clientId,
            'client secret' => $this->clientSecret,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'telephone' => $request->telephone
        ]);     
        
        return $response->json();
        
    } 
    
    public function login(OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->post($this->adminurl."/api/rest/login", [
            'email' => $request->email,
            'password' => $request->password
        ]);     
        
        return $response->json();
        
    }
    
    public function forgottenPassword(OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->post($this->adminurl."/api/rest/forgotten", [
            'email' => $request->email
        ]);     
        
        return $response->json();
        
    }      
    
    public function changePassword(OCRequest $request)
    {

        $response = Http::withHeaders([
            'Authorization' => $this->authorization
        ])->put($this->adminurl."/api/rest/account/password", [
            'password' => $request->password,
            'confirm' => $request->confirm
        ]);     
        
        return $response->json();
        
    }      
    
    public function runETL()
    {

        $response = Http::get($this->adminurl.'/products.php');
        
        $etlResponse = $response->json(); 
 
        
        if ($this->isOptionAlreadyExists("statistics") == false) {
            
            $id = DB::table('options')->insertGetId(
                ['option_key' => 'statistics', 'option_value' => serialize($etlResponse)]
            );   
            
        } else {
            
            $affected = DB::table('options')
              ->where('option_key', 'statistics')
              ->update(['option_value' => serialize($etlResponse)]);
            
        }
        
        return ["Record synchronized"];
        

     
    }  
    
    public function isOptionAlreadyExists($optionKey) {
        
        $result = DB::select("SELECT COUNT(option_id) as total FROM `options` WHERE option_key = '$optionKey'");
        
        
        if ($result[0]->total > 0) {
            return true;
        } else {
            return false;
        }
        
    }



}

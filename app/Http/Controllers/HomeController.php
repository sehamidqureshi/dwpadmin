<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        
        $data = $this->loadDashboardStatistics();
        
        $productsCount = $data['success']['products'];
        $customersCount = $data['success']['customers'];
        
        
        return view('dashboard',['productsCount'=>$productsCount,'customersCount'=>$customersCount]);
    }
    
    public function showEcommerceDashboard() {
        
        $data = $this->loadDashboardStatistics();
        
        $productsCount = $data['success']['products'];
        $customersCount = $data['success']['customers'];
        
        
        return view('dashboard',['productsCount'=>$productsCount,'customersCount'=>$customersCount]);        
    }
        
    public function showWebsiteDashboard() {
        
        $data = $this->loadDashboardStatistics();
        
        $productsCount = $data['success']['products'];
        $customersCount = $data['success']['customers'];
        
        
        return view('dashboard',['productsCount'=>$productsCount,'customersCount'=>$customersCount]);         
        
        
    }
    
    public function showTicketsListing() {
        
        $data = $this->loadDashboardStatistics();
        
        $productsCount = $data['success']['products'];
        $customersCount = $data['success']['customers'];
        
        
        return view('tickets',['productsCount'=>$productsCount,'customersCount'=>$customersCount]);            
        
        
    }
    
    public function loadDashboardStatistics() {
        
        $result = DB::select("SELECT option_value FROM `options` WHERE option_key = 'statistics'");
        
        if (isset($result[0]->option_value)) {
            
            return unserialize($result[0]->option_value);
            
        } else {
            return array();
        }
    }
    
}

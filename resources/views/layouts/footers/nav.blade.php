<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-12">
        <div class="copyright text-center text-muted">
            &copy; {{ now()->year }} <a href="https://dwp.com.pk" class="font-weight-bold ml-1" target="_blank">DWP Group</a>
            
        </div>
    </div>
    <div class="col-xl-6">
        
    </div>
</div>
@extends('layouts.app')

@section('content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            
           
        </div>
    </div>
</div>
    
    
    <div class="container-fluid mt--7">
   
        <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Recent Tickets</h3>
                            </div>
                            
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Subject</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Priority</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">
                                        Need assistance on newly purchased item.
                                    </th>
                                   <td>
                                      <span class="badge badge-dot mr-4">
                                        <i class="bg-success"></i>
                                        <span class="status">completed</span>
                                      </span>
                                    </td>
                                    <td>
                                        20 Aug 2019
                                    </td>
                                    <td>
                                        <i class="fas fa-arrow-up text-success mr-3"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Having issue in article 34A in Inverter - EcoStar
                                    </th>
                                     <td>
                                      <span class="badge badge-dot mr-4">
                                        <i class="bg-warning"></i>
                                        <span class="status">pending</span>
                                      </span>
                                    </td>
                                    <td>
                                        21 Jan 2019
                                    </td>
                                    <td>
                                        <i class="fas fa-arrow-down text-warning mr-3"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                    Having issue in outer. Where do I consult kindly let me know.
                                    </th>
                                     <td>
                                      <span class="badge badge-dot mr-4">
                                        <i class="bg-info"></i>
                                        <span class="status">delayed</span>
                                      </span>
                                    </td>
                                    <td>
                                        12 Mar 2020
                                    </td>
                                    <td>
                                        <i class="fas fa-arrow-down text-warning mr-3"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        2 Kits are missing in Gree Products recently purchesed.
                                    </th>
                                    <td>
                                      <span class="badge badge-dot mr-4">
                                        <i class="bg-success"></i>
                                        <span class="status">completed</span>
                                      </span>
                                    </td>
                                    <td>
                                        30 Sep 2018
                                    </td>
                                    <td>
                                        <i class="fas fa-arrow-up text-success mr-3"></i>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">
                                        Is there any policy for returning items or exchange with old one?
                                    </th>
                                    <td>
                                      <span class="badge badge-dot mr-4">
                                        <i class="bg-danger"></i>
                                        <span class="status">on schedule</span>
                                      </span>
                                    </td>
                                    <td>
                                        15 Jan 2020
                                    </td>
                                    <td>
                                        <i class="fas fa-arrow-down text-danger mr-3"></i>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush